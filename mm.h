#ifndef MM_H
#define MM_H

typedef sruct metadata_block *p_block;
struct metadata_block
{
	size_t size;
	p_block next;
	int free;
};
void* Malloc(size_t size);
void free(void* ptr);
void* realloc(void* ptr,size_t size);

#endif

