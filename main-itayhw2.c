#include "ceaser.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
	int i;
	if (argc < 4)
	{
		printf("usage: %s <mode> <message> <key>\n",argv[0]);
		exit(1);
	}
	system("PAUSE");
	for ( i = 0; i < 5; i++)
	{
		printf("Argv[%d] = %s\n", i, argv[i]);
	}
	int mode = (argv[1][0] == 'd') ? -1 : 1;
	printf("%d", mode);
	
	int key = atoi(argv[4]);
	printf("%d", key);
	char * input = shift_string(argv[2],mode * key);

	printf("%s\n",input);
	
	return 0;
}
